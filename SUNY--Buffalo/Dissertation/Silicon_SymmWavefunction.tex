% \documentclass{report}

% \usepackage{graphicx} \usepackage{dcolumn} \usepackage{bm}
% \usepackage{amsfonts, amssymb, amsmath} \usepackage{braket}

%\newcommand{\ket}[1]{\textstyle \left| #1 \right\rangle \displaystyle}
%\newcommand{\bra}[1]{\textstyle \left\langle #1 \right| \displaystyle}
%\newcommand{\braket}[2]{\textstyle \left\langle #1 \left|  #2
%\right\rangle\right. \displaystyle} \newcommand{\im}{\mathrm{i}}
%\newcommand{\e}{\e} \newcommand{\pwrt}[2]{\frac{\partial #1}{\partial #2}}
%\newcommand{\diff}{\mathrm{d}}
%
%\begin{document}

\subsection{Symmetry concerns within the silicon wavefunctions.}

To understand the symmetrization of our full wavefunction, we apply our
projection symmetrization operator to a general wavefunction and find what
conditions on that wavefunction must apply to make the wavefunction correspond
to one of the symmetries of silicon.
\begin{equation}
  \mathcal{V}_{\kappa,i}^{\dagger}
  \Psi
  =
  \left(\alpha_{0}^{\left(\kappa,i\right)}\right)^{*}
  \left(xyz\right)
  \Psi
  +
  \left(\alpha_{1}^{\left(\kappa,i\right)}\right)^{*}
  \left(zxy\right)
  \Psi
  +
  \left(\alpha_{2}^{\left(\kappa,i\right)}\right)^{*}
  \left(yzx\right)
  \Psi.
\end{equation}
We can define a set of ancillary functions based off an initial function. We
describe these functions as being "valley indexed" by a particular
crystallographic axis. 
\begin{align}
  f_{z} & \equiv \left(xyz\right)f, \label{idx:z}\\
  f_{x} & \equiv \left(yzx\right)f, \label{idx:x}\\
  f_{y} & \equiv \left(zxy\right)f. \label{idx:y}
\end{align}
It is useful to note these ancillary functions have the following relationships,
\begin{align}
  f_{z} & = \left(zxy\right)f_{x} = \left(yzx\right)f_{y}, \\
  f_{x} & = \left(yzx\right)f_{z} = \left(zxy\right)f_{y}, \\
  f_{y} & = \left(zxy\right)f_{z} = \left(yzx\right)f_{x}.
\end{align}
For ease of nomenclature, we'll call these three transformations
$\left(xyz\right)$, $\left(yzx\right)$, and $\left(zxy\right)$ the indexing
transformations. 

Since we've introduced the concept of valley indexed functions, we'll introduce
an additional stipulation upon $\Psi$, namely that
\begin{equation}
  \Psi = c_{z}\psi_{z} + c_{x}\psi_{x} + c_{y}\psi_{y}
\label{wf_raw}
\end{equation}
When we apply the first factor operator of the symmetrization projection
operator to the above form, we'll find different results depending on the
particular symmetry we are projecting upon. For $\kappa = A,E$ we can note the
following properties upon the $\alpha$ coefficients.
\begin{align}
  \alpha_{0} &= \frac{1}{3}, \\
  \alpha_{1}^{2} &= \frac{\alpha_{2}}{3}, \\
  \alpha_{2}^{2} &= \frac{\alpha_{1}}{3}, \\
  \alpha_{1}^{*} &= \alpha_{2}, \\
  \alpha_{2}^{*} &= \alpha_{1}, \\
  \alpha_{1}\alpha_{2} &= \frac{1}{9}.
\end{align}
For such a state we find the following 
\begin{equation}
  \mathcal{V}_{\kappa,i}^{\dagger}
  \Psi
  =
  \left(c_{z}+c_{x}\alpha_{2}+c_{y}\alpha_{1}\right)
  \left(
    \alpha_{0}^{\left(\kappa,i\right)}\psi_{z}
    +
    \alpha_{1}^{\left(\kappa,i\right)}\psi_{x}
    +
    \alpha_{2}^{\left(\kappa,i\right)}\psi_{y}
  \right)
  .
\tag{\ref{wf_raw}a}
\label{wf_symm:a}
\end{equation}
This shows a clear projection form, with a resulting symmetrized function form
multiplied by a overall factor indicating how much of the original function can
be said to "lie along" the symmetrized form. For $\kappa = A,E$, then, we can
take the $c$ coefficients to have the following form, $c_{q} = C \alpha_{ q }^{
\left(\kappa,i\right) }$, where $C$ is a complex factor useful for
normalization. If we define the $c_{q}$ coefficients as such, we'll find that
\begin{equation}
  \mathcal{V}_{\kappa,i}^{\dagger}
  \Psi
  =
  \Psi
\end{equation}

In the case of $\kappa = T$, the $\alpha$ coefficients have the following,
different properties;
\begin{align}
  \alpha_{0} &= \delta_{iz}, \\
  \alpha_{1} &= \delta_{ix}, \\
  \alpha_{2} &= \delta_{iy}, \\
  \alpha_{q}^{2} &= \alpha_{q}, \\
  \alpha_{q}^{*} &= \alpha_{q}, \\
  \alpha_{q}\alpha_{r} &= \delta_{iq}\delta_{qr}.
\end{align}
This yields the following effect upon our wavefunction, namely
\begin{align}
  \mathcal{V}_{T_{n},i}^{\dagger}
  \Psi
  & =
  d_{z}\psi_{z}
  +
  d_{x}\psi_{x}
  +
  d_{y}\psi_{y}
  ,
\tag{\ref{wf_raw}b}
\label{wf_symm:b}
\\
  d_{z}
  & =
  \delta_{iz}c_{z}
  +
  \delta_{ix}c_{x}
  +
  \delta_{iy}c_{y}
  ,
\\
  d_{x}
  & =
  \delta_{iz}c_{x}
  +
  \delta_{ix}c_{y}
  +
  \delta_{iy}c_{z}
  ,
\\
  d_{y}
  & =
  \delta_{iz}c_{y}
  +
  \delta_{ix}c_{z}
  +
  \delta_{iy}c_{x}
  .
\end{align}
Alternatively, we can state
\begin{align}
  \mathcal{V}_{T_{n},i}^{\dagger}
  \Psi
  & =
  \delta_{iz}\psi_{0}
  +
  \delta_{ix}\psi_{1}
  +
  \delta_{iy}
  \psi_{2}
  ,
\tag{\ref{wf_raw}c}
\label{wf_symm2}
\\
  \psi_{0} & = c_{z}\psi_{z}+c_{x}\psi_{x}+c_{y}\psi_{y},
\\
  \psi_{1} & = c_{x}\psi_{z}+c_{y}\psi_{x}+c_{z}\psi_{y},
\\
  \psi_{2} & = c_{y}\psi_{z}+c_{z}\psi_{x}+c_{x}\psi_{y}
  .
\end{align}
We see a marked difference between the effect of the
$\mathcal{V}_{T_{n},i}^{\dagger}$ factor operator and the
$\mathcal{V}_{A_{n},i}^{\dagger}$, $\mathcal{V}_{E,i}^{\dagger}$ operators. For
$\kappa = T$, in essence the effect of the valley mixing/rotation factor
operator $\mathcal{V}_{T_{n},i}^{\dagger}$ is to generate a cyclic permutation
of the $c$ coefficients without an overall change of function form. Another way
to interpret the effect of the $\mathcal{V}_{T_{n},i}^{\dagger}$ factor operator
is that it aligns the $z$-axis of the function with the $T$-states' axes of
symmetry. In the case of $\kappa = A,E$ we find that the factor operator
redistributes the weight of each valley.

We can expect the following to be true for all symmetries,
\begin{equation}
  \mathcal{V}_{\kappa,i}^{\dagger}
  \Psi
  =
  \beta_{z}^{\left(\kappa,i\right)}\psi_{z}
  +
  \beta_{x}^{\left(\kappa,i\right)}\psi_{x}
  +
  \beta_{y}^{\left(\kappa,i\right)}\psi_{y}.
\tag{\ref{wf_raw}d}
\label{multi-term_form}
\end{equation}
Our full wavefunctions will include terms with Bloch function factors, so we
expect the terms of the full wavefunction to themselves be valley-indexed. We
can group those terms by axes, and so we expect the following
\begin{equation}
  \psi_{q} = \psi_{\parallel,q} + \psi_{\perp_{1},r} + \psi_{\perp_{2},s},
\label{single-term:form}
\end{equation}
or alternatively
\begin{equation}
  \psi_{q}
  =
  \psi_{\aleph_{qz}, z} + \psi_{\aleph_{qx},x} + \psi_{\aleph_{qy},y}.
\tag{\ref{single-term:form}a}
\label{single-term:form-a}
\end{equation}
with possible values for $\aleph_{qq^{\prime}} = \parallel$,$\perp_{1}$,or
$\perp_{2}$. We also expect these valley-indexed functions to include terms with
Bloch function factors. To wit, we expect
\begin{equation}
  \psi_{\aleph,q}
  =
  \Phi_{\aleph,+q}\eta^{\left(+q\right)}
  +
  \Phi_{\aleph,-q}\eta^{\left(-q\right)}.
\label{single-axis:form0}
\end{equation}
We can group the terms solely by valley, which will give us 
\begin{align}
  \Psi
  &=
  \psi_{z}^{\prime} + \psi_{x}^{\prime} + \psi_{y}^{\prime},
\tag{\ref{single-term:form}b}
\label{single-term:form-b}
\\
  \psi_{z}^{\prime}
  &=
  \beta_{z}^{\left(\kappa,i\right)}\psi_{\parallel,z}
  +
  \beta_{y}^{\left(\kappa,i\right)}\psi_{\perp_{1},z}
  +
  \beta_{x}^{\left(\kappa,i\right)}\psi_{\perp_{2},z}
  ,
\label{single-axis:form:z}
\\
  \psi_{x}^{\prime}
  &=
  \beta_{x}^{\left(\kappa,i\right)}\psi_{\parallel,x}
  +
  \beta_{z}^{\left(\kappa,i\right)}\psi_{\perp_{1},x}
  +
  \beta_{y}^{\left(\kappa,i\right)}\psi_{\perp_{2},x}
  ,
\label{single-axis:form:x}
\\
  \psi_{y}^{\prime}
  &=
  \beta_{y}^{\left(\kappa,i\right)}\psi_{\parallel,y}
  +
  \beta_{x}^{\left(\kappa,i\right)}\psi_{\perp_{1},y}
  +
  \beta_{z}^{\left(\kappa,i\right)}\psi_{\perp_{2},y}
  .
\label{single-axis:form:y}
\end{align}
We can rewrite this expression to make the underlying envelope-Bloch product
form explicit.
\begin{align}
  \psi_{q}^{\prime}
  &=
  \sum_{\lambda = +,-}
  \Phi_{\lambda q}\eta^{\left(\lambda q\right)}
  ,
\label{single-axis:form1}
\\
  \Phi_{\lambda z}
  & =
  \beta_{z}^{\left(\kappa,i\right)}\Phi_{\parallel,\lambda z}
  +
  \beta_{y}^{\left(\kappa,i\right)}\Phi_{\perp_{1},\lambda z}
  +
  \beta_{x}^{\left(\kappa,i\right)}\Phi_{\perp_{2},\lambda z},
\\
  \Phi_{\lambda x}
  & =
  \beta_{x}^{\left(\kappa,i\right)}\Phi_{\parallel,\lambda x}
  +
  \beta_{z}^{\left(\kappa,i\right)}\Phi_{\perp_{1},\lambda x}
  +
  \beta_{y}^{\left(\kappa,i\right)}\Phi_{\perp_{2},\lambda x}
\\
  \Phi_{\lambda y}
  & =
  \beta_{y}^{\left(\kappa,i\right)}\Phi_{\parallel,\lambda y}
  +
  \beta_{x}^{\left(\kappa,i\right)}\Phi_{\perp_{1},\lambda y}
  +
  \beta_{z}^{\left(\kappa,i\right)}\Phi_{\perp_{2},\lambda y}
\end{align}
It's worth noting that the symmetrizing operators can be extended down through
the layers of definition introduced in \eqref{single-term:form} and
\eqref{single-axis:form0} to the envelope functions $\Phi_{\aleph,\pm q}$
because the operators are linear. We see the $\beta$ coefficients show up in our
expressions above. The value of the $\beta$ coefficients depend upon the value
of the symmetry index $\kappa$ and follow directly from \eqref{wf_symm:a} and
\eqref{wf_symm:b}. There's an important distinction between $\beta$ for $\kappa
= T$ and $\beta$ for $\kappa = A,E$. For the $A,E$ states the coefficients are
not arbitrary, with $\beta_{q} = \alpha_{q}$. For the $T$ states, the value of
$\beta_{q} = d_{q}$ and thus depends upon the value of the $c$ arbitrary
constants. Since the factor operator for $\kappa = T$ does not remix the
valleys, the weight of each valley remains arbitrary. As such we note that the
envelope functions for states with $\kappa = T$ are not linked by the indexing
expressions introduced in \eqref{idx:z}, \eqref{idx:x}, and \eqref{idx:y}. This
is not the case for $\kappa = A,E$. Namely, we find the following
valley-indexing relationships.
\begin{align}
  \Phi_{\lambda x}
  &=
  3\alpha_{1}^{\left(\kappa,i\right)}\left(yzx\right)
  \Phi_{\lambda z},
\label{idx:env:xz}
\\
  \Phi_{\lambda y}
  &=
  3\alpha_{2}^{\left(\kappa,i\right)}\left(zxy\right)
  \Phi_{\lambda z},
\label{idx:env:yz}
\\
  \Phi_{\lambda y}
  &=
  3\alpha_{1}^{\left(\kappa,i\right)}\left(yzx\right)
  \Phi_{\lambda x}.
\label{idx:env:yx}
\end{align}

Over the course of this section, we will determine the necessary properties of
the envelope function to generate the desired symmetry. As we apply the rest of
the projection operator factors to our full wavefunction, we'll explore how the
interplay of the Bloch and envelope functions influences the process of
symmetrization.

Our next step will be to apply $\mathcal{A}_{z}^{\left(\kappa\right)}$ to our
wavefunction. We note that for $\kappa = E$, the operator
$\mathcal{A}_{z}^{\left(E\right)} = \left(xyz\right)$ and so the effect of the
operator is trivial. For $\kappa = A,T$, the effect of this operator upon our
terms are given below.

Without much trouble we can assert that
\begin{equation}
  \mathcal{A}_{z}^{\left(\kappa\right)}\psi_{z}^{\prime} = \psi_{z}^{\prime}.
\end{equation}
Given the linearity of the symmetry transformations, we can achieve this on a
per Bloch function term basis, yielding
\begin{equation} 
  \mathcal{A}_{z}^{\left(\kappa\right)}\Phi_{\lambda z} = \Phi_{\lambda z}.
\label{acyc:env}
\end{equation}
if we also assert of our envelope function terms that
\begin{equation}
  \Phi_{\lambda z}
  =
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda z}
  .
\label{acyc:z}
\end{equation}
However, there are subtleties when applying the acyclic operator
$\mathcal{A}_{z}$ to states with Bloch function factors indexed by $x$ and $y$.
Let's consider the following.
\begin{align}
  \mathcal{A}_{z}^{\left(\kappa\right)}
  \Phi_{\lambda x}\eta^{\left(\lambda x\right)}
  & =
  \frac
  {
    \Phi_{\lambda x}
    \eta^{\left(\lambda x\right)}
    + 
    \left[\sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda x}\right]
    \eta^{\left(\lambda y\right)}
  }
  {2},
  \\
  \mathcal{A}_{z}^{\left(\kappa\right)}
  \Phi_{\lambda y}\eta^{\left(\lambda y\right)}
  & =
  \frac
  {
    \Phi_{\lambda y}
    \eta^{\left(\lambda y\right)}
    + 
    \left[\sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda y}\right]
    \eta^{\left(\lambda x\right)}
  }
  {2}.
\end{align}
Here we note that for $\kappa = T$ the envelope functions are not linked by the
indexing relationships. Since there are no indexing relationships between the
$\Phi_{\lambda x}$ and $\Phi_{\lambda y}$ for $\kappa = T$ we must stop here,
yielding
\begin{equation}
  \mathcal{A}_{z}^{\left(T\right)}
  \left\{ 
    \Phi_{\lambda x}\eta^{\left(\lambda x\right)}
    +
    \Phi_{\lambda y}\eta^{\left(\lambda y\right)}
  \right\}
= 
\left[
  \frac
  {
    \Phi_{\lambda x}
    +
    \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda y}
  }
  {2}
\right]\eta^{\left(\lambda x\right)}
+
\left[
  \frac
  {
    \Phi_{\lambda y}
    +
    \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda x}
  }
  {2}
\right]\eta^{\left(\lambda y\right)}
\end{equation}
Matching terms yields
\begin{align}
  \Phi_{\lambda x}
  &=
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda y}
\label{acyc:x-y}
\\
  \Phi_{\lambda y}
  &=
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda x}
\label{acyc:y-x}
\end{align}

However, we saw above in \eqref{idx:env:xz}, \eqref{idx:env:yz}, and
\eqref{idx:env:yx} that relationships exist between the envelope functions of
different valley-indices for $\kappa = A,E$. In particular, it may be shown that
\begin{align}
  \left(yxz\right)\Phi_{\lambda x}
  & =
  3\alpha_{2}^{\left(\kappa,i\right)}\sigma^{\left(\kappa\right)}
  \Phi_{\lambda y}
  ,
\label{acyc:x}
\\
  \left(yxz\right)\Phi_{\lambda y}
  & =
  3\alpha_{1}^{\left(\kappa,i\right)}\sigma^{\left(\kappa\right)}
  \Phi_{\lambda x}
  .
\label{acyc:y}
\end{align}
Since the $E$-states have a trivial acyclic operator, we'll keep our focus on
the $A$-states. In light of this consideration, we find
$3\alpha_{1}^{\left(A\right)} = 3\alpha_{2}^{\left(A\right)} = 1$ we can assert
that
\begin{equation}
  \mathcal{A}_{z}^{\left(A_{n}\right)}
  \left\{
    \Phi_{\lambda x}\eta^{\left(\lambda x\right)}
    +
    \Phi_{\lambda y}\eta^{\left(\lambda y\right)}
  \right\}
  = 
  \Phi_{\lambda x}\eta^{\left(\lambda x\right)}
  +
  \Phi_{\lambda y}\eta^{\left(\lambda y\right)}
  .
  \label{acyc:env:xy}
\end{equation}
We also observe that
\begin{equation}
  \left(yzx\right)\left(yxz\right)\left(yzx\right)
=
  \left(zxy\right)\left(yxz\right)\left(zxy\right)
=
  \left(yxz\right).
\end{equation}
As such, it is fairly trivial to prove that \eqref{acyc:z}, \eqref{acyc:x}, and
\eqref{acyc:y} are equivalent relationships for functions linked by the
valley-index relations. 

While the above arguments were explicitly selected to satisfy the requirements
for $\kappa = A$, we find it convenient to use variations of the same underlying
naive envelope function for all symmetric states. As such, we require our naive
envelope states to satisfy \eqref{acyc:z}.

Now we should apply the double inversion factor operator $\mathcal{D}_{z}$ to
the wavefunction. To ensure symmetrization we wish to allow
\begin{align}
  \mathcal{D}_{z}^{\left(\kappa\right)}\psi_{q}^{\prime}
  &=
  \psi_{q}^{\prime}
  .
\label{dblinv}
\end{align}
For \eqref{dblinv} to be true for $q = z$, it must be the case that
\begin{equation}
  \Phi_{\lambda z}
  =
  \left(\frac{\left(xyz\right)+\left(\bar{x}\bar{y}z\right)}{4}\right)
  \Phi_{\lambda z}
  +
  \omega^{\left(\kappa\right)}
  \left(
    \frac
    { \left(\bar{x}y\bar{z}\right)+\left(x\bar{y}\bar{z}\right) }
    {4}
  \right)
  \Phi_{-\lambda z}
  .
\label{dblinv:env:z}
\end{equation}
Some further arithmetic work on \eqref{dblinv:env:z} can establish the following
relationships. We can show that
\begin{align}
  \Phi_{\lambda z}
  & =
  \left(\bar{x}\bar{y}z\right)
  \Phi_{\lambda z}
  ,
\label{dblinv:z:para}
\\
  \Phi_{-\lambda z}
  & =
  \omega^{\left(\kappa\right)}
  \left(
    \frac{\left(\bar{x}y\bar{z}\right)+\left(x\bar{y}\bar{z}\right)}{2}
  \right)
  \Phi_{\lambda z}
  .
\label{dblinv:z:anti}
\end{align}

We saw in \eqref{acyc:env:xy} that the presence of $x$-valley terms implies the
presence of $y$-valley terms in our partially symmetrized wavefunction, and vice
versa. Thus, for the valleys perpendicular to $z$ we must have both
\begin{equation}
  \Phi_{\lambda x}
  =
  \left(
    \frac
    {
      \left(xyz\right)
      +
      \omega^{\left(\kappa\right)}\left(x\bar{y}\bar{z}\right)
    }
    {4}
  \right)
  \Phi_{\lambda x}
  +
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega^{\left(\kappa\right)}\left(\bar{x}y\bar{z}\right)
    }
    {4}
  \right)
  \Phi_{-\lambda x}
  ,
\label{dblinv:x}
\end{equation}
and
\begin{equation}
  \Phi_{\lambda y}
  =
  \left(
    \frac
    {
      \left(xyz\right)
      +
      \omega^{\left(\kappa\right)}\left(\bar{x}y\bar{z}\right)
    }
    {4}
  \right)
  \Phi_{\lambda y}
  +
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega^{\left(\kappa\right)}\left(x\bar{y}\bar{z}\right)
    }
    {4}
  \right)
  \Phi_{-\lambda y}
  .
\label{dblinv:y}
\end{equation}
As before we can do some arithmetic work to simplify these expressions. If we
assume that \eqref{dblinv:x} and \eqref{dblinv:y} are both true we find
\begin{align}
  \Phi_{\lambda x}
  & =
  \omega^{\left(\kappa\right)}
  \left(x\bar{y}\bar{z}\right)
  \Phi_{\lambda x}
  ,
\label{dblinv:x:para}
\\
  \Phi_{-\lambda x}
  & =
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega^{\left(\kappa\right)}\left(\bar{x}y\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\lambda x}
  ,
\label{dblinv:x:anti}
\end{align}
as well as
\begin{align}
  \Phi_{\lambda y}
  & =
  \omega^{\left(\kappa\right)}
  \left(\bar{x}y\bar{z}\right)
  \Phi_{\lambda y}
  ,
\label{dblinv:y:para}
\\
  \Phi_{-\lambda y}
  & =
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega^{\left(\kappa\right)}\left(x\bar{y}\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\lambda y}
  .
\label{dblinv:y:anti}
\end{align}
As before, we can use the indexing relationships to try to find the
equivalencies between the above relationships. For the $\kappa = A,E$ states, we
once again invoke \eqref{idx:env:xz}, \eqref{idx:env:yz}, and
\eqref{idx:env:yx}. In that spirit, note the following
\begin{align}
  \left(zxy\right)\left(x\bar{y}\bar{z}\right)\left(yzx\right)
&=
  \left(\bar{x}\bar{y}z\right)
\\
\left(yzx\right)\left(\bar{x}y\bar{z}\right)\left(zxy\right)
&=
  \left(\bar{x}\bar{y}z\right)
\end{align}
Applying these relationships to \eqref{dblinv:x:para} and \eqref{dblinv:y:para}
yields
\begin{equation}
  \Phi_{\lambda z}
  =
  \omega^{\left(\kappa\right)}
  \left(\bar{x}\bar{y}z\right)
  \Phi_{\lambda z}
  .
\label{dblinv:xy:para}
\end{equation}

For both \eqref{dblinv:z:para} and \eqref{dblinv:xy:para} to be true, it must be
the case that
\begin{equation}
  \omega^{\left(\kappa\right)} = +1.  \label{AE_cond}
\end{equation}
This condition is satisfied for states with $\kappa = A,E$.

We can also examine the relationships between \eqref{dblinv:z:anti},
\eqref{dblinv:x:anti}, and \eqref{dblinv:y:anti}. Examining the symmetry
operators and their products, we assert that
\begin{align}
  \left(zxy\right)\left(\bar{x}\bar{y}z\right)\left(yzx\right)
  & = 
  \left(\bar{x}y\bar{z}\right)
\\
  \left(zxy\right)\left(\bar{x}y\bar{z}\right)\left(yzx\right)
  & = 
  \left(x\bar{y}\bar{z}\right)
\\
  \left(yzx\right)\left(\bar{x}\bar{y}z\right)\left(zxy\right)
  & = 
  \left(x\bar{y}\bar{z}\right)
\\
  \left(yzx\right)\left(x\bar{y}\bar{z}\right)\left(zxy\right)
  & = 
  \left(\bar{x}y\bar{z}\right)
\end{align}
Applying these relationships to \eqref{dblinv:x:anti}, and \eqref{dblinv:y:anti}
we find
\begin{align}
  \Phi_{-\lambda z}
  & =
  \left(
    \frac
    {
      \left(\bar{x}y\bar{z}\right)
      +
      \omega^{\left(\kappa\right)}
      \left(x\bar{y}\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\lambda z}
  ,
\tag{\ref{dblinv:x:anti}a}
\\
  \Phi_{-\lambda z}
  & =
  \left(
    \frac
    {
      \left(x\bar{y}\bar{z}\right)
      +
      \omega^{\left(\kappa\right)}
      \left(\bar{x}y\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\lambda z}
  ,
\tag{\ref{dblinv:y:anti}a}
\end{align}
We note that if \eqref{AE_cond} is true, then \eqref{dblinv:z:anti},
\eqref{dblinv:x:anti}, and \eqref{dblinv:y:anti} are equivalent. 

So far we have found the partially symmetrized wavefunction to have the
following form, namely
\begin{equation}
  \mathcal{D}_{z}^{\left(\kappa\right)}
  \mathcal{A}_{z}^{\left(\kappa\right)}
  \mathcal{V}_{\kappa,i}^{\dagger}
  \psi
  =
  \psi_{x}^{\prime}
  +
  \psi_{y}^{\prime}
  +
  \psi_{z}^{\prime}
  .
\tag{\ref{wf_raw}f}
\label{multi-aleph:form1}
\end{equation}
Applying the $\mathcal{D}_{z}$ double inversion operator didn't alter the
overall structure of our wavefunction, but exerted conditions upon the
underlying envelope functions. 

It can be shown that the application of the final factor operator $\mathcal{V}$
returns the wavefunction to its initial form. To ensure equality we find the
following requirements upon our envelope functions. For the $\kappa = A,E$
states the following requirements are sufficient.
\begin{align}
  \Phi_{\lambda z}
  &=
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda z}
  .
\tag{\ref{acyc:z}}
\\
  \Phi_{\lambda z}
  & =
  \left(\bar{x}\bar{y}z\right)
  \Phi_{\lambda z}
  ,
\tag{\ref{dblinv:z:para}}
\\
  \Phi_{-\lambda z}
  & =
  \left(
    \frac{\left(\bar{x}y\bar{z}\right)+\left(x\bar{y}\bar{z}\right)}{2}
  \right)
  \Phi_{\lambda z}
  .
\tag{\ref{dblinv:z:anti}}
\end{align}
Due to the valley mixing of the $A$- and $E$-states the envelope functions of
other valleys may be found from the following indexing relationships.
\begin{align}
  \Phi_{\lambda x}
  &=
  3\alpha_{1}^{\left(\kappa,i\right)}\left(yzx\right)
  \Phi_{\lambda z}
  ,
\tag{\ref{idx:env:xz}}
\\
  \Phi_{\lambda y}
  &=
  3\alpha_{2}^{\left(\kappa,i\right)}\left(zxy\right)
  \Phi_{\lambda z}
  .
\tag{\ref{idx:env:yz}}
\end{align}

For the $\kappa = T$ states, the symmetry picks out a longitudinal axis, with
the other axes being perpendicular. The longitudinal envelope functions are
independent of the transverse envelope functions. We can state the requirements
on these envelope functions as such. For the longitudinal envelopes we see
\begin{align}
  \Phi_{\lambda z}
  &=
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda z}
  .
\tag{\ref{acyc:z}}
\\
  \Phi_{\lambda z}
  & =
  \left(\bar{x}\bar{y}z\right)
  \Phi_{\lambda z}
  ,
\tag{\ref{dblinv:z:para}}
\\
  \Phi_{-\lambda z}
  & =
  -
  \left(
    \frac{\left(\bar{x}y\bar{z}\right)+\left(x\bar{y}\bar{z}\right)}{2}
  \right)
  \Phi_{\lambda z}
  .
\tag{\ref{dblinv:z:anti}}
\end{align}
The transverse envelopes are independent of the longitudinal envelopes, but not
independent of each other. The relationship between the transverse envelopes
follows 
\begin{equation}
  \Phi_{\lambda x}
  =
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda y}.
\tag{\ref{acyc:x-y}}
\end{equation}
The transverse envelopes must also follow
\begin{align}
  \Phi_{\lambda x}
  & =
  \omega^{\left(\kappa\right)}
  \left(x\bar{y}\bar{z}\right)
  \Phi_{\lambda x}
  ,
\tag{\ref{dblinv:x:para}}
\\
  \Phi_{-\lambda x}
  & =
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      -
      \left(\bar{x}y\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\lambda x}
  .
\tag{\ref{dblinv:x:anti}}
\end{align}