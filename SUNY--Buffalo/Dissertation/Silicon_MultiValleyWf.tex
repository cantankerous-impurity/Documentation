% \documentclass{report}

% \usepackage{graphicx}
% \usepackage{dcolumn}
% \usepackage{bm}
% \usepackage{amsfonts, amssymb, amsmath}
% \usepackage{braket}

%\newcommand{\ket}[1]{\textstyle \left| #1 \right\rangle \displaystyle}
%\newcommand{\bra}[1]{\textstyle \left\langle #1 \right| \displaystyle}
%\newcommand{\braket}[2]{\textstyle \left\langle #1 \left|  #2 \right\rangle\right. \displaystyle}
%\newcommand{\im}{\mathrm{i}}
%\newcommand{\e}{\mathrm{e}}
%\newcommand{\pwrt}[2]{\frac{\partial #1}{\partial #2}}
%\newcommand{\diff}{\mathrm{d}}

%\begin{document}

\subsection{ Full symmetrized wavefunction, including envelope function
    anisotropy }
\label{section:full_wavefunctions}

This section is a collection of information derived in previous sections, and is
collected here to make referencing of the form of our wavefunctions for various
symmetries swift and convenient for users and maintainers of the software.

We have a few preliminaries to define. We may start with what we in this
document call the \emph{axial Bloch function}. In our system, the Bloch
functions we are most concerned are the six energy minima of the conduction band
of bulk silicon. Each lies along a particular crystallographic $k$-space axis,
which may be labeled $x$, $y$, and $z$. We denote the location in $k$-space where each minima is located as follows
\begin{equation}
    \bm{k}_{\pm q} = \pm k_{0}\hat{q},
\end{equation}
where
\begin{equation}
    k_{0} = \eta_{\text{Si}}\left(\frac{2\pi}{a_{\text{Si}}}\right).
\end{equation}
Above $a_{\text{Si}} = \SI{.5431020511}{\nano\metre}$ is the lattice parameter of the silicon crystal, and $\eta_{\text{Si}} \approx .85$.

We use the following notation to describe one of these valley Bloch functions
\begin{equation}
    \eta^{\left(+q\right)} = \e^{\im\bm{k}_{\pm q}\cdot\bm{r}}
    u_{\bm{k}_{q}\bm{r}}.
\end{equation}
In the previous section we found that our choice of envelope function
basis, along with certain parameters specifying functions within that basis,
allowed us to factor the envelope function from the Bloch functions of opposite
valleys. These factored out Bloch function summations have the following form
\begin{equation}
    \eta^{\left(q\right)}_{\xi}
    =
        \eta^{\left(+q\right)}
            +
        \xi
        \eta^{\left(-q\right)}.
\end{equation}
We describe this form as the \emph{axial Bloch function}.

There is a feature of notation below we should discuss; previously, we defined
the $m$ index of functions in our basis to extend over the nonnegative integers.
Because some of the basis functions must be set to zero depending on the parity
of $m$, we introduce new notation to reflect this. Namely, $m_{e}$ extends over
the nonegative even integers, including zero. Whereas, $m_{o}$ should be
understood to extend over the nonnegative odd integers. When invoked without
subscript, $m$ retains its original domain.

Now, as we build up our multi-valley wavefunctions we consider what we will
describe here as the \emph{longitudinal single-axis basis wavefunction}. We
found that, for $\left(\kappa,i\right) = \left(A_{n},0\right)$,
$\left(E,i\right)$, and $\left(T_{n},z\right)$ the part of the wavefunction
lying along the $z$-axial Bloch function have the following forms
\begin{align}
    \psi_{\parallel_{0},m_{e}lk\sigma}^{\left(\kappa,z\right)}
        & \coloneqq
        \Phi_{\parallel_{0},m_{e}lk\sigma}^{\left(\kappa,z\right)}
        \eta^{\left(z\right)}_{\Omega^{\left(\kappa\right)}_{m_{e}l\sigma}}
\tag{\ref{single-axis:longitudinal:z:0}a}
,\\
    \psi_{\parallel_{1},mlk\sigma}^{\left(\kappa,z\right)}
        & \coloneqq
        \Phi_{\parallel_{1},mlk\sigma}^{\left(\kappa,z\right)}
        \eta^{\left(z\right)}_{\omega^{\left(\kappa\right)}\left(-1\right)^{m}}
\tag{\ref{single-axis:longitudinal:z:1}a}
.
\end{align}
The envelope basis functions associated with the basis wavefunctions above are constructed from our naive envelope basis functions and go as follows
\begin{align}
    \Phi_{\parallel_{0},m_{e}lk\sigma}^{\left(\kappa,z\right)}
        &=
            \phi_{klm_{e}\sigma;\left(-1\right)^{m_{e}/2}}
            ^{(z)}
,\\
    \Phi_{\parallel_{1},mlk\sigma}^{\left(\kappa,z\right)}
        &=
          \phi_{klm\sigma;\left(-1\right)^{l}}^{(x)}
          +
          \omega^{\left(\kappa\right)}\Omega^{\left(\kappa\right)}_{ml\sigma}
          \phi_{klm\sigma;\left(-1\right)^{l+m}}^{(y)}
\tag{\ref{env:long:z:1}a}
.
\end{align}
Above we introduced the following constant.
\begin{equation}
\Omega^{\left(\kappa\right)}_{ml\sigma}
    \coloneqq
        \sigma\omega^{\left(\kappa\right)}
        \left(-1\right)^{l+m}\im^{+m}.
\end{equation}
The values of this constant goes as $\Omega = \pm 1$ for $m_{e}$, and $\Omega =
\pm \im$ for $m_{o}$.

We have enough information now to construct the full wavefunctions for the
$\kappa = A,E$ symmetries. For those states, we can invoke \eqref{idx:env:xz}
and \eqref{idx:env:yz} to find the single-axis wavefunctions for the $x$ and $y$
axes. In essence the idea of a "longitudinal axis" is irrelevant for states of
these symmetries -- the three axes are in some sense interchangeable.
Following this line of reasoning and using the equations mentioned above we can
state the forms of the full $A$, $E$ wavefunctions to be
\begin{align}
    \Psi_{\varrho,m_{e}lk}^{\left(A_{n}\right)}
        &=
            \frac{1}{3}
            \psi_{\varrho,m_{e}lk\sigma^{\left(A_{n}\right)}}
            ^{\left(A_{n},z\right)}
                +
            \frac{1}{3}
            \psi_{\varrho,m_{e}lk\sigma^{\left(A_{n}\right)}}
            ^{\left(A_{n},x\right)}
                +
            \frac{1}{3}
            \psi_{\varrho,m_{e}lk\sigma^{\left(A_{n}\right)}}
            ^{\left(A_{n},y\right)}
\label{wavefunction:A}
,\\
    \Psi_{\varrho,m_{e}lk\sigma}^{\left(E,0\right)}
        &=
            \frac{1}{3}
            \psi_{\varrho,m_{e}lk\sigma}^{\left(E,z\right)}
                +
            \frac{\tau}{3}
            \psi_{\varrho,m_{e}lk\sigma}^{\left(E,x\right)}
                +
            \frac{\tau^{2}}{3}
            \psi_{\varrho,m_{e}lk\sigma}^{\left(E,y\right)}
\label{wavefunction:E:0}
,\\
    \Psi_{\varrho,m_{e}lk\sigma}^{\left(E,1\right)}
        &=
            \frac{1}{3}
            \psi_{\varrho,m_{e}lk\sigma}^{\left(E,z\right)}
                +
            \frac{\tau^{2}}{3}
            \psi_{\varrho,m_{e}lk\sigma}^{\left(E,x\right)}
                +
            \frac{\tau}{3}
            \psi_{\varrho,m_{e}lk\sigma}^{\left(E,y\right)}
\label{wavefunction:E:1}
.
\end{align}
Above $\tau = -\frac{1}{2} + \im\frac{\sqrt{3}}{2}$, one of the third roots of
unity. The $A$-type states are nondegenerate, so we drop the degeneracy index
above. We retain the degeneracy index for the $E$-type states. We define the $x$
and $y$ single-axis wavefunctions simply as $\psi^{\left(x\right)} =
\left(yzx\right)\psi^{\left(z\right)}$ and $\psi^{\left(y\right)} =
\left(zxy\right)\psi^{\left(z\right)}$. These cyclic transformations
straightforwardly swap the $z$ index in the above expressions for the $x$ and
$y$ expressions respectively.

The $T$-states are triply degenerate, and each state may be valley indexed.
These states do have a longitudinal axis matching the valley index. In the
previous section we were able to find an expression for the perpendicular
single-axis wavefunctions. To wit, we found
\begin{align}
    \Psi_{\parallel_{0},m_{e}lk}^{\left(T_{n},z\right)}
        &=
            \psi_{\parallel_{0},m_{e}lk\sigma^{\left(T_{n}\right)}}
            ^{\left(T_{n},z\right)}
,\\
    \Psi_{\parallel_{1},mlk}^{\left(T_{n},z\right)}
        &=
            \psi_{\parallel_{1},mlk\sigma^{\left(T_{n}\right)}}
            ^{\left(T_{n},z\right)}
,\\
    \Psi_{\perp_{0},m_{o}lk\zeta}^{\left(T_{n},z\right)}
        &=
            \phi_{m_{o}lk\zeta}^{\left(T_{n},x\right)}
            \eta^{\left(x\right)}_{\zeta\left(-1\right)^{l}}
            -
            \zeta\sigma^{\left(T_{n}\right)}\im^{+m}
            \phi_{m_{o}lk\bar{\zeta}}^{\left(T_{n},y\right)}
            \eta^{\left(y\right)}_{\zeta\left(-1\right)^{l}}
,\\
    \Psi_{\perp_{1},mlk}^{\left(T_{n},z\right)}
        &=
            \phi_{mlk\left(-1\right)^{l+1}}^{\left(T_{n},y\right)}
            \eta^{\left(x\right)}_{\left(-1\right)^{m+1}}
            +
            \Omega^{\left(\kappa\right)}_{ml\sigma^{\left(T_{n}\right)}}
            \phi_{mlk\left(-1\right)^{l+m+1}}^{\left(T_{n},x\right)}
            \eta^{\left(y\right)}_{\left(-1\right)^{m+1}}
,\\
    \Psi_{\perp_{2},mlk}^{\left(T_{n},z\right)}
        &=
            \phi_{mlk\left(-1\right)^{l+m+1}}^{\left(T_{n},z\right)}
            \eta^{\left(x\right)}_{\left(-1\right)^{m}}
            +
            \left(-1\right)^{m}
            \Omega^{\left(\kappa\right)}_{ml\sigma^{\left(T_{n}\right)}}
            \phi_{mlk\left(-1\right)^{l+1}}^{\left(T_{n},z\right)}
            \eta^{\left(y\right)}_{\left(-1\right)^{m}}
.
\end{align}
Any of the two longitudinal and three perpendicular forms can be used for the
$z$-axis of the $T$-states. For the other axes, we can once again use the cyclic transformations to swap valley axes.

% We define the following as our \emph{naive envelope basis function} with
% anisotropy included as follows,
% \begin{equation}
%   \Phi_{klm;\zeta;\eta\lambda;\bm{r}}^{(Wq)}
%   \coloneqq
%   F_{lk;\eta\lambda;r\chi_{q}}^{\left(W\right)}
%   P_{lm;\lambda;\chi_{q}}
%   Q_{m\zeta;\phi_{q}}
%   ,
% \tag{\ref{env:naive}}
% \end{equation}