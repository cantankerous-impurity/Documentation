% \documentclass{report}

% \usepackage{graphicx} \usepackage{dcolumn} \usepackage{bm}
% \usepackage{amsfonts, amssymb, amsmath} \usepackage{braket}

%\newcommand{\ket}[1]{\textstyle \left| #1 \right\rangle \displaystyle}
%\newcommand{\bra}[1]{\textstyle \left\langle #1 \right| \displaystyle}
%\newcommand{\braket}[2]{\textstyle \left\langle #1 \left|  #2
%\right\rangle\right. \displaystyle} \newcommand{\im}{\mathrm{i}}
%\newcommand{\e}{\e} \newcommand{\pwrt}[2]{\frac{\partial #1}{\partial #2}}
%\newcommand{\diff}{\mathrm{d}}
%
%\begin{document}

\subsection{Silicon envelope functions and single valley wavefunctions.}

With this discussion on the overall wavefunction and its implications upon the
envelope functions in mind, we examine our envelope function basis. In appendix
\ref{appendix:transformations} we explored the transformation properties of the
state defined below,
\begin{equation}
  \phi_{klm;\eta\lambda;\bm{r}}^{(Wq)}
  =
  F_{lk;\eta\lambda;r\chi_{q}}^{\left(W\right)}
  P_{lm;\lambda;\chi_{q}}
  Q_{m;\phi_{q}}
  ,
\end{equation}
where each part may be defined as below. The naive radial part is defined in
\ref{subsection:variational-anisotropy} in equation
\eqref{scaled_wf:radial_part}
\begin{equation}
  F_{kl;\eta;r}^{\left(W\right)}
  =
  \sqrt{\frac{W}{\eta^{3}}}
  L_{k;\rho^{W}}^{\left(\alpha\right)}
  \mathrm{e}^{-\frac{\rho^{W}}{2}}
  \rho^{l},
  \quad\quad
  \rho = \frac{r}{\eta}
  .
  \tag{\ref{naive_wf:radial_part}}
\end{equation}
In the equation above, $l$ and $k$ are nonnegative integers and $\eta > 0$ is a
variational radial scaling parameter. The function $L$ is a generalized Laguerre
polynomial of $k$-th order and $\alpha = \frac{2l + 3 - W}{W}$. We expect $W =
1$ or $2$.

When variational anisotropy $\chi_{q} \rightarrow \lambda \chi_{q}$ is
introduced, we can define a new "radial" function as follows, by introducing a
scaling of the radial parameter that goes as $r \rightarrow r
\vartheta_{\lambda\chi_{q}}$. This emerges as a modification of the radial
parameters $\eta$ with angular dependence. We may write it as
\begin{equation}
  F_{lk;\eta\lambda;r\chi_{q}}^{\left(W\right)}
  = 
  F_{lk;\eta_{\lambda\chi_{q}};r}^{\left(W\right)}
  \quad\quad
  \eta_{\lambda;\chi_{q}}
  =
  \frac{\eta}{\vartheta_{\lambda\chi_{q}}}
  .
  \tag{\ref{scaled_wf:radial_part}}
\end{equation}
The "longitudinal" part can be defined as below, with the function $P$ being the
associated Legendre polynomials with degree $l$ and order $m$. $m$ is
nonnegative and $l$ is an integer such that $l \ge m$.
\begin{equation}
  P_{lm;\lambda;\chi_{q}}
  =
  \sqrt{ \frac{\lambda}{\vartheta_{\lambda\chi_{q}}^{3}} }
  P_{lm;\chi_{\lambda}^{\left(q\right)}}
  .
  \tag{\ref{scaled_wf:long_part}}
\end{equation}
The azimuthal part of the envelope basis function are unaffected by the
introduction of the variational anisotropy. As discussed in Appendix
\ref{appendix:transformations} the standard definition of the spherical
harmonics has been changed to better accommodate the symmetry of our system.
\begin{equation}
  Q_{m\zeta;\phi_{q}}
  \equiv
  \delta_{m0}
  \left( 1 - \delta_{\zeta\bar{1}} \right)
  % \left( \frac{ 1 + \zeta }{ 2 } \right)
  +
  \left(1-\delta_{m0}\right)
  \left(
    \frac{ \e^{\im m \phi_{q}} + \zeta\e^{-\im m \phi_{q}} }{  \sqrt{2} }
  \right)
  .
  \tag{\ref{scaled_wf:azim_part}}
\end{equation}
As before $m$ is a nonnegative integer. We consider $\zeta$ complex such that
$\left|\zeta\right| = 1$.

We define the following as our \emph{naive envelope basis function} with
anisotropy included as follows,
\begin{equation}
  \phi_{klm\zeta;\eta\lambda;\bm{r}}^{(Wq)}
  \equiv
  F_{lk;\eta\lambda;r\chi_{q}}^{\left(W\right)}
  P_{lm;\lambda;\chi_{q}}
  Q_{m\zeta;\phi_{q}}
  ,
\label{env:naive}
\end{equation}
Earlier, we introduced the concept of valley-indexing. We see of course that
$\Phi^{\left(z\right)}$ can be indexed by $z$. However, we should also note that
the combination $\Phi^{\left(x\right)} \pm
\sigma\left(yxz\right)\Phi^{\left(y\right)}$ can also be indexed by $z$ in the
same sense as $\Phi^{\left(z\right)}$. As such, we shall consider the following
generic form for our valley-indexed envelope function.
\begin{equation}
  \Phi_{+q}
  \equiv
  a_{z}^{\left(q\right)}
  \phi_{ \zeta_{z}^{\left(q\right)} }^{\left(z\right)}
    +
  a_{x}^{\left(q\right)}
  \phi_{ \zeta_{x}^{\left(q\right)} }^{\left(x\right)}
    +
  a_{y}^{\left(q\right)}
  \phi_{ \zeta_{y}^{\left(q\right)} }^{\left(y\right)}
\label{env:full:q}
\end{equation}  
We can examine the effect of the transformations that form the $\mathcal{A}_{z}$
and $\mathcal{D}_{z}^{\left(\kappa\right)}$ factor operators upon our naive
envelope basis function defined in \eqref{env:naive}. For such a basis function,
we previously elucidated in Appendix \ref{appendix:transformations} the effects
of the transformations under consideration. To wit, for the $z$-index we found
\begin{align}
  \zeta\im^{-m}
  \phi_{ \left(-1\right)^{m}\zeta^{*} }^{\left(z\right)}
  & =
    \left(yxz\right)
    \phi_{ \zeta }^{\left(z\right)}
\tag{\ref{acyc:z-state:z}}
,\\
\zeta\left(-1\right)^{l+m}
\phi_{ \zeta^{*} }^{\left(z\right)}
& =
    \left(x\bar{y}\bar{z}\right)
    \phi_{ \zeta }^{\left(z\right)},
\tag{\ref{dblinv:x-state:z}}
\\
  \left(-1\right)^{m}
  \phi_{ \zeta }^{ \left(z\right) }
  & =
    \left(\bar{x}\bar{y}z\right)
    \phi_{ \zeta }^{\left(z\right)}
\tag{\ref{dblinv:z-state:z}}
,\\
  \zeta\left(-1\right)^{l}
  \phi_{ \zeta^{*} }^{\left(z\right)}
  & =
    \left(\bar{x}y\bar{z}\right)
    \phi_{ \zeta }^{\left(z\right)}
\tag{\ref{dblinv:y-state:z}}
.
\end{align}
For the $y$-index functions we found
\begin{align}
  \zeta\im^{-m}
  \phi_{ \left(-1\right)^{m}\zeta^{*} }^{\left(x\right)}
  & =
    \left(yxz\right)
    \phi_{ \zeta }^{\left(y\right)}
\tag{\ref{acyc:z-state:y}}
,\\
  \zeta\left(-1\right)^{l}
  \phi_{ \zeta^{*} }^{\left(y\right)}
  & =
    \left(x\bar{y}\bar{z}\right)
    \phi_{ \zeta }^{\left(y\right)},
\tag{\ref{dblinv:x-state:y}}
\\
  \zeta\left(-1\right)^{l+m}
  \phi_{ \zeta^{*} }^{\left(y\right)}
  & =
    \left(\bar{x}\bar{y}z\right)
    \phi_{ \zeta }^{\left(y\right)}
\tag{\ref{dblinv:z-state:y}}
,\\
  \left(-1\right)^{m}
  \phi_{ \zeta }^{\left(y\right)}
  & =
      \left(\bar{x}y\bar{z}\right)
      \phi_{ \zeta }^{\left(y\right)}
\tag{\ref{dblinv:y-state:y}}
.
\end{align}
Finally, for the $x$-index functions we found
\begin{align}
  \zeta\im^{-m}
  \phi_{ \left(-1\right)^{m}\zeta^{*} }^{\left(y\right)}
  & =
    \left(yxz\right)
    \phi_{ \zeta }^{\left(x\right)}
\tag{\ref{acyc:z-state:x}}
,\\
  \left(-1\right)^{m}
  \phi_{ \zeta }^{\left(x\right)}
  & =
    \left(x\bar{y}\bar{z}\right)
    \phi_{ \zeta }^{\left(x\right)},
\tag{\ref{dblinv:x-state:x}}
\\
  \zeta\left(-1\right)^{l}
  \phi_{ \zeta^{*} }^{\left(x\right)}
  & =
    \left(\bar{x}\bar{y}z\right)
    \phi_{ \zeta }^{\left(x\right)}
\tag{\ref{dblinv:z-state:x}}
,\\
  \zeta\left(-1\right)^{l+m}
  \phi_{ \zeta^{*} }^{\left(x\right)}
  & =
    \left(\bar{x}y\bar{z}\right)
    \phi_{ \zeta }^{\left(x\right)}
\tag{\ref{dblinv:y-state:x}}
.
\end{align}

In the previous section, we determined that our $z$-indexed envelope functions
must obey these conditions,
\begin{align}
  \Phi_{\lambda z}
  & =
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda z}
  ,
\tag{\ref{acyc:z}}
\\
  \Phi_{\lambda z}
  & =
  \left(\bar{x}\bar{y}z\right)
  \Phi_{\lambda z}
  .
\tag{\ref{dblinv:z:para}}
\end{align}
For $\kappa = A,E$ the envelope functions valley-indexed by $x$ and $y$ may be
found from the $z$- indexed function via \eqref{idx:env:xz} and
\eqref{idx:env:yz}. For $\kappa = T$ we also need to define independent $x$- and
$y$-indexed envelopes. They will follow these relationships.
\begin{align}
  \Phi_{\lambda y}
  &=
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda x}
  \tag{\ref{acyc:y-x}}
,\\
  \Phi_{\lambda x}
  &=
  \sigma^{\left(\kappa\right)}\left(yxz\right)\Phi_{\lambda y}
  \tag{\ref{acyc:x-y}}
,\\
  \Phi_{\lambda x}
  & =
  -
  \left(x\bar{y}\bar{z}\right)
  \Phi_{\lambda x}
\tag{\ref{dblinv:x:para}}
,\\
  \Phi_{\lambda y}
  & =
  -
  \left(\bar{x}y\bar{z}\right)
  \Phi_{\lambda y}
\tag{\ref{dblinv:y:para}}
.
\end{align}
It can be shown quite directly that \eqref{acyc:y-x} and \eqref{acyc:x-y} are
equivalent. Additionally, if \eqref{acyc:y-x} is true then it can be shown that
that \eqref{dblinv:x:para} and \eqref{dblinv:y:para} are also equivalent. As
such, we can examine the implications of just \eqref{acyc:x-y} and
\eqref{dblinv:x:para}.

Let's start by exploring the implications of \eqref{acyc:z} and
\eqref{dblinv:z:para} upon \eqref{env:full:q} for $q = z$. In particular we can
set $\zeta_{z}^{\left(z\right)} = \sigma \im^{+m}$. This yields
\begin{align}
  \left(yxz\right)
  \phi_{ \sigma\im^{+m} }^{\left(z\right)}
  & =
    \sigma
    \phi_{ \sigma\im^{+m} }^{\left(z\right)}
,\\
  \left(\bar{x}\bar{y}z\right)
  \phi_{ \sigma\im^{+m} }^{\left(z\right)}
  & =
    \left(-1\right)^{m}
    \phi_{ \sigma\im^{+m} }^{ \left(z\right) }
.
\end{align}
For $\kappa = A,T$ we can satisfy equation \eqref{acyc:z} if we set $\sigma =
\sigma^{\left(\kappa\right)}$. For $\kappa = E$ we recall that
$\sigma^{\left(\kappa\right)} = 0$. For $E$-states we can take $\sigma = \pm 1$
as a free index, similar to $m$, $l$, and $k$. We note that
\eqref{dblinv:z:para} can only be satisfied for $\Phi^{\left(z\right)}$ when $m$
is even. When $m$ is odd, we are unable to construct an appropriate envelope
function from $\Phi_{ \sigma\im^{+m} }^{\left(z\right)}$. As such, we'll set
\begin{equation}
  a_{z}^{\left(z\right)}
    \rightarrow
  \left(\frac{1+\left(-1\right)^{m}}{2}\right)a_{z}^{\left(z\right)}
\label{state:z-a:z-constraint}
\end{equation}
Moving forward, we consider the implications of \eqref{dblinv:z:para} and
\eqref{acyc:z} upon the $x$- and $y$-indexed functions. To begin, let's examine
the implications of \eqref{dblinv:z:para} and \eqref{acyc:z} upon our
$x,y$-indexed naive envelopes, namely
\begin{align}
  \left(\bar{x}\bar{y}z\right)
  \phi_{ \left(-1\right)^{l} }^{\left(x\right)}
  & =
    \phi_{ \left(-1\right)^{l} }^{\left(x\right)}
,\\
  \left(\bar{x}\bar{y}z\right)
  \phi_{ \left(-1\right)^{l+m} }^{\left(y\right)}
  & =
    \phi_{ \left(-1\right)^{l+m} }^{\left(y\right)}
,\\
  \sigma\left(yxz\right)
  \phi_{ \left(-1\right)^{l} }^{\left(x\right)}
  & =
    \sigma\left(-1\right)^{l}\im^{-m}
    \phi_{ \left(-1\right)^{l+m} }^{\left(y\right)}
  ,\\
  \sigma\left(yxz\right)
  \phi_{ \left(-1\right)^{l+m} }^{\left(y\right)}
  & =
    \sigma\left(-1\right)^{l+m}\im^{-m}
    \phi_{ \left(-1\right)^{l} }^{\left(x\right)}
.
\end{align}
Above, we set $\zeta_{x}^{\left(z\right)}= \left(-1\right)^{l}$ and
$\zeta_{y}^{\left(z\right)}= \left(-1\right)^{l+m}$. At this juncture we find it
convenient to define the following constant,
\begin{equation}
  \Omega \equiv \sigma\omega\left(-1\right)^{l+m}\im^{+m}.
\end{equation}
Next, consider the following function. 
\begin{equation}
  \phi_{\sigma}^{\left(yx\right)}
  =
    \phi_{ \left(-1\right)^{l} }^{\left(x\right)}
    +
    \omega\Omega
    \phi_{ \left(-1\right)^{l+m} }^{\left(y\right)}
    .
\label{env:long:z:1}
\end{equation}
This function obeys \eqref{dblinv:z:para} and \eqref{acyc:z} with no caveats.
\begin{align}
  \phi_{\sigma}^{\left(yx\right)}
  & =
    \sigma
    \left(yxz\right)
    \phi_{\sigma}^{\left(yx\right)}
  ,
\\
  \phi_{\sigma}^{\left(yx\right)}
  & =
    \left(\bar{x}\bar{y}z\right)
    \phi_{\sigma}^{\left(yx\right)}
    .
\end{align}
We note that if we set our coefficients in \eqref{env:full:q} for $q = z$ to go
as 
\begin{align}
  a_{x}^{\left(z\right)}
    &\rightarrow
  a_{xy}^{\left(z\right)},
\\
  a_{y}^{\left(z\right)}
    &\rightarrow
    \omega\Omega
  a_{xy}^{\left(z\right)},
\end{align}
we find that our $+z$ envelope functions goes as
\begin{equation}
  \Phi_{+z}
  =
  a_{z}^{\left(q\right)}
  \left(\frac{1+\left(-1\right)^{m}}{2}\right)
  \phi_{ \sigma\im^{+m} }^{\left(z\right)}
    +
  a_{xy}^{\left(q\right)}
  \phi_{\sigma}^{\left(yx\right)}
\end{equation}
Note that the coefficients $a^{\left(z\right)}$ in $\Phi_{+z}$ above remain
arbitrary. As such, for our purposes we find it convenient within our software
methodology to treat these terms as different states within our basis. To that
end, we can write our $z$-indexed envelope functions as
\begin{align}
  \Phi_{+z;\sigma}
    &=
  \left(\frac{1+\left(-1\right)^{m}}{2}\right)
  \phi_{ \sigma\im^{+m} }^{\left(z\right)}
,\\
  \Phi_{+z;\sigma}^{\prime}
    &=
  \phi_{\sigma}^{\left(yx\right)}
.
\end{align}
Recall that for $\kappa = A,T$ the index $\sigma = \pm 1$ is set by the
symmetry, while for $\kappa = E$ it is a free index.

Moving forward, we can utilize \eqref{dblinv:z:anti} as a definition. Namely, we
can define the negative valley index envelope functions as
\begin{equation}
  \Phi_{-z}
  \equiv
    \omega^{\left(\kappa\right)}
    \left(
      \frac{\left(\bar{x}y\bar{z}\right)+\left(x\bar{y}\bar{z}\right)}{2}
    \right)
    \Phi_{+z}
    .
\end{equation}
Applying these to our envelope basis functions yields
\begin{align}
  \omega
  \left(
    \frac
    {
      \left(\bar{x}y\bar{z}\right)
      +
      \left(x\bar{y}\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{+z;\sigma}
  & =
    \Omega
    \Phi_{+z;\sigma}
,\\
\omega
\left(
  \frac
  {
    \left(\bar{x}y\bar{z}\right)
    +
    \left(x\bar{y}\bar{z}\right)
  }
  {2}
\right)
\Phi_{+z;\sigma}^{\prime}
  & =
    \omega
    \left(-1\right)^{m}
    \Phi_{+z;\sigma}^{\prime}
.
\end{align}
Having defined our $z$-indexed envelope function we can introduce
\emph{single-axis} wavefunctions, combining the single-valley wavefunctions of
opposite valleys lying along one axis. To that end, we can introduce the following \emph{axial Bloch function},
\begin{equation}
  \eta^{\left(q\right)}_{\xi}
    =
      \eta^{\left(+q\right)} + \xi \eta^{\left(-q\right)}
\end{equation}
We write our $z$-axis wavefunction, including the axial Bloch functions.
\begin{align}
  \psi_{\sigma\omega}^{\left(z\right)}
  & \equiv
    \left(\frac{1+\left(-1\right)^{m}}{2}\right)
    \phi_{ \sigma\im^{+m} }^{\left(z\right)}
    \eta^{\left(z\right)}_{\Omega}
\label{single-axis:longitudinal:z:0}
,\\
  {\psi_{\sigma\omega}^{\left(z\right)}}^{\prime}
  & \equiv
    \phi_{ \sigma }^{\left(yx\right)}
    \eta^{\left(z\right)}_{\omega\left(-1\right)^{m}}
.
\label{single-axis:longitudinal:z:1}
\end{align}
This forms the core of the definition of our single valley states for $\kappa =
A, E$. We can use the cyclic transformations given in \eqref{idx:env:xz} and
\eqref{idx:env:yz} along with the above definitions for the $z$-axis
wavefunctions to yield the envelope basis functions and single-axis
wavefunctions for the other axes within those symmetries.

However, for the $\kappa = T$ states, the $x$- and $y$-indexed perpendicular
envelope functions cannot be found from the $z$-indexed longitudinal envelope
function. We invoke \eqref{acyc:x-y} and \eqref{dblinv:x:para} to specify
conditions upon our perpendicular envelopes and again utilize \eqref{env:full:q}
as our general form.

With this in mind we will explore the qualities of forms suitable for
$\Phi_{+x}$ and $\Phi_{+y}$. In particular, let's consider the effect of the
operator used in \eqref{dblinv:x:para} upon our naive envelope functions.
\begin{align}
  \omega\left(x\bar{y}\bar{z}\right)
  \phi_{ \zeta_{x}^{\left(x\right)} }^{\left(x\right)}
  & =
    \omega\left(-1\right)^{m}
    \phi_{ \zeta_{x}^{\left(x\right)} }^{\left(x\right)}
\tag{\ref{dblinv:x-state:x}a}
\label{dblinv:x-state:x-a}
,\\
  \omega\left(x\bar{y}\bar{z}\right)
  \phi_{ \zeta_{y}^{\left(x\right)} }^{\left(y\right)}
    & =
    \omega\zeta_{y}^{\left(x\right)} \left(-1\right)^{l}
    \phi_{ \left\{\zeta_{y}^{\left(x\right)}\right\}^{*} }^{\left(y\right)}
\tag{\ref{dblinv:x-state:y}a}
,\\
  \omega\left(x\bar{y}\bar{z}\right)
  \phi_{ \zeta_{z}^{\left(x\right)}  }^{\left(z\right)}
    & =
      \omega\zeta_{z}^{\left(x\right)}\left(-1\right)^{l+m}
      \phi_{ \left\{\zeta_{z}^{\left(x\right)}\right\}^{*} }^{\left(z\right)}
\tag{\ref{dblinv:x-state:z}a}
.
\end{align}
In light of the above information, for the terms in $\Phi_{+x}$ we set
$\zeta_{z}^{\left(x\right)} = \omega\left(-1\right)^{l+m}$ and
$\zeta_{y}^{\left(x\right)} = \omega\left(-1\right)^{l}$. We can also note that
for \eqref{dblinv:x-state:x-a} it must be the case for the $x$-indexed term that
$\omega\left(-1\right)^{m} = 1$ or $\Phi^{\left(x\right)} = 0$. This can be
accomplished by the same strategy as in \eqref{state:z-a:z-constraint}. In light
of the relationships above, we can state
\begin{align}
  \Phi_{+x}
  & =
  a_{x}^{\left(x\right)}
  \Phi_{+x}^{\left(x\right)}
    +
  a_{y}^{\left(x\right)}
  \Phi_{+x}^{\left(y\right)}
    +
  a_{z}^{\left(x\right)}
  \Phi_{+x}^{\left(z\right)}
\\
  \Phi_{+x}^{\left(x\right)}
  & = 
  \left(\frac{1+\omega\left(-1\right)^{m}}{2}\right)
  \phi_{ \zeta_{x}^{\left(x\right)} }^{\left(x\right)},
\\
  \Phi_{+x}^{\left(y\right)}
  & = 
  \phi_{ \omega\left(-1\right)^{l} }^{\left(y\right)},
\\
  \Phi_{+x}^{\left(z\right)}
  & = 
  \phi_{ \omega\left(-1\right)^{l+m} }^{\left(z\right)}
\end{align}

As we turn to the acyclic properties of the state, we can take \eqref{acyc:y-x}
as definitional. Let's examine the effect of the acyclic swap upon our naive
envelope functions. Since we have already determined
$\zeta_{y}^{\left(x\right)}$ and $\zeta_{z}^{\left(x\right)}$ we will insert
them in appropriate places in the expressions below. This yields
\begin{align}
  \sigma\left(yxz\right)
  \phi_{ \zeta_{x}^{\left(x\right)} }^{\left(x\right)}
    & =
      \zeta_{x}^{\left(x\right)}\sigma\im^{-m}
      \phi_{ \left(-1\right)^{m}\zeta_{x}^{\left(x\right)} }^{\left(y\right)}
\tag{\ref{acyc:z-state:x}}
,\\
  \sigma\left(yxz\right)
  \phi_{ \omega\left(-1\right)^{l} }^{\left(y\right)}
    & =
      \Omega
      \phi_{ \omega\left(-1\right)^{l+m} }^{\left(x\right)}
\tag{\ref{acyc:z-state:y}a}
,\\
\sigma\left(yxz\right)
\phi_{ \omega\left(-1\right)^{l+m} }^{\left(z\right)}
  & =
    \left(-1\right)^{m}\Omega
    \phi_{ \omega\left(-1\right)^{l} }^{\left(z\right)}
\tag{\ref{acyc:z-state:z}a}
.
\end{align}
We note that none of the above conditions defining the envelope functions
determine $\zeta_{x}^{\left(x\right)}$. It is convenient for our purposes, with
some loss of generality, to have $\zeta_{x}^{\left(x\right)}$ real. We will see
how this simplifies the expression of the $\psi_{-x}$ and $\psi_{-y}$, in
particular allowing the Bloch and envelope functions to be factored apart from
each other. We do this to simplify the integration expression, helping both
analytical and numerical computation of the integrals required to compute the
Hamiltonian matrix elements. Since the $\zeta$ indices for the other functions
have been determined at this point, we may drop the upper and lower valley
indices on $\zeta_{x}^{\left(x\right)}$ without ambiguity.

Considering the above expressions involving our naive envelope basis functions
yield the following expressions for our $y$-indexed perpendicular envelopes.
Since the coefficients in this expression are shared between the $x$- and
$y$-valley indexed envelope, the upper valley index on the coefficients are
superfluous and can be dropped. For the terms of $y$-valley indexed envelope we
find
\begin{align}
  \Phi_{+y}
  & =
  a_{y}
  \Phi_{+y}^{\left(x\right)}
    +
  a_{x}
  \Phi_{+y}^{\left(y\right)}
    +
  a_{z}
  \Phi_{+y}^{\left(z\right)}
\\
  \Phi_{+y}^{\left(x\right)}
  & = 
  \Omega
  \phi_{ \omega\left(-1\right)^{l+m} }^{\left(x\right)},
\\
  \Phi_{+y}^{\left(y\right)}
  & = 
  \left(\frac{1+\omega\left(-1\right)^{-m}}{2}\right)
  \omega\zeta\sigma\im^{+m}
  \phi_{ \omega\zeta }^{\left(y\right)},
\\
  \Phi_{+y}^{\left(z\right)}
  & = 
  \left(-1\right)^{m}
  \Omega
  \phi_{ \omega\left(-1\right)^{l} }^{\left(z\right)}
\end{align}
We can use what we've learned to find $\Phi_{-x}$ and $\Phi_{-y}$. To wit, we
apply our $x$-indexed opposite valley operator to the naive envelope basis
states with appropriate $\zeta$ indices, yielding
\begin{align}
  \Phi_{-x}^{\left(x\right)}
& \equiv
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega
      \left(\bar{x}y\bar{z}\right)
    }
    {2}
  \right)
  \phi_{\zeta}^{\left(x\right)}
  = 
  \zeta\left(-1\right)^{l}
  \left( \frac{ 1 + \omega\left(-1\right)^{m} }{2} \right)
  \phi_{\zeta}^{\left(x\right)}
,\\
\Phi_{-x}^{\left(y\right)}
& \equiv
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega
      \left(\bar{x}y\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\omega\left(-1\right)^{l}}^{\left(y\right)}
  =
  \omega\left(-1\right)^{m}
  \phi_{\omega\left(-1\right)^{l}}^{\left(y\right)}
,\\
\Phi_{-x}^{\left(z\right)}
& \equiv
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega
      \left(\bar{x}y\bar{z}\right)
    }
    {2}
  \right)
  \phi_{\omega\left(-1\right)^{l+m}}^{\left(z\right)}
=
  \left(-1\right)^{m}
  \phi_{\omega\left(-1\right)^{l+m}}^{\left(z\right)}
.
\end{align}

We must also apply our $y$-indexed opposite valley operator, firstly to the
naive envelope basis states with appropriate $\zeta$ indices, yielding
\begin{align}
\Phi_{-y}^{\left(x\right)}
&\propto
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega
      \left(x\bar{y}\bar{z}\right)
    }
    {2}
  \right)
  \Phi_{\omega\left(-1\right)^{l+m}}^{\left(x\right)}
  = 
  \omega\left(-1\right)^{m}
  \phi_{\omega\left(-1\right)^{l+m}}^{\left(x\right)}
,\\
\Phi_{-y}^{\left(y\right)}
&\propto
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega
      \left(x\bar{y}\bar{z}\right)
    }
    {2}
  \right)
  \phi_{\omega\zeta}^{\left(y\right)}
  =
  \zeta\left(-1\right)^{l}
  \left( \frac{ 1 + \omega\left(-1\right)^{m} }{2} \right)
  \phi_{\omega\zeta}^{\left(y\right)}
,\\
\Phi_{-y}^{\left(z\right)}
&\propto
  \left(
    \frac
    {
      \left(\bar{x}\bar{y}z\right)
      +
      \omega
      \left(x\bar{y}\bar{z}\right)
    }
    {2}
  \right)
  \phi_{\omega\left(-1\right)^{l}}^{\left(z\right)}
  =
  \left(-1\right)^{m}
  \phi_{\omega\left(-1\right)^{l}}^{\left(z\right)}
.
\end{align}

It is useful at this juncture to recognize that $a_{x}$, $a_{y}$, and $a_{z}$
are independent coefficients. As before, it is useful to separate these terms as
independent basis functions. To that end, we write
\begin{align}
  \psi_{\perp_{0}}^{\left(T,z\right)}
    &=
      \left(\frac{1-\left(-1\right)^{m}}{2}\right)
      \left(
        \phi_{\zeta}^{\left(x\right)}
        \eta^{\left(x\right)}_{\zeta\left(-1\right)^{l}}
        -
        \zeta\sigma\im^{+m}
        \phi_{-\zeta}^{\left(y\right)}
        \eta^{\left(y\right)}_{\zeta\left(-1\right)^{l}}
      \right)
,\\
  \psi_{\perp_{1}}^{\left(T,z\right)}
    &=
      \phi_{-\left(-1\right)^{l}}^{\left(y\right)}
      \eta^{\left(x\right)}_{-\left(-1\right)^{m}}
      +
      \Omega
      \phi_{-\left(-1\right)^{l+m}}^{\left(x\right)}
      \eta^{\left(y\right)}_{-\left(-1\right)^{m}}
,\\
  \psi_{\perp_{2}}^{\left(T,z\right)}
    &=
      \phi_{-\left(-1\right)^{l+m}}^{\left(z\right)}
      \eta^{\left(x\right)}_{\left(-1\right)^{m}}
      +
      \left(-1\right)^{m}\Omega
      \phi_{-\left(-1\right)^{l}}^{\left(z\right)}
      \eta^{\left(y\right)}_{\left(-1\right)^{m}}
.
\end{align}
Above we recall that for $\kappa = T$ states, $\omega^{\left(T\right)} =
-1$.