% % % \documentclass[aps, prl, preprint]{revtex4-2}
% % \documentclass{report}

% % \usepackage{graphicx}
% % \usepackage{dcolumn}
% % \usepackage{array,multirow,adjustbox}
% % \usepackage{bm}
% % \usepackage{amsfonts, amssymb, amsmath}
% % \usepackage{braket}
% % \usepackage{siunitx}

% % % \newcommand{\ket}[1]{\textstyle \left| #1 \right\rangle \displaystyle}
% % % \newcommand{\bra}[1]{\textstyle \left\langle #1 \right| \displaystyle}
% % % \newcommand{\braket}[2]{\textstyle \left\langle #1 \left|  #2 \right\rangle\right. \displaystyle}

% % \let\originalleft\left
% % \let\originalright\right
% % \renewcommand{\left}{\mathopen{}\mathclose\bgroup\originalleft}
% % \renewcommand{\right}{\aftergroup\egroup\originalright}

% % \newcommand{\im}{\mathrm{i}}
% % \newcommand{\e}{\mathrm{e}}
% % \newcommand{\pwrt}[2]{\frac{\partial #1}{\partial #2}}
% % \newcommand{\diff}{\mathrm{d}}

% % \DeclareMathOperator{\atantwo}{atan2}

% % \title{An open source software package to compare and extend effective mass models of the phosphorous donor in silicon}
% % \author{Luke Pendo}
% % \date{}

% % \begin{document}

\subsection{Kohn-Luttinger and donor states in silicon}

In a 1955 paper\cite{KohnLuttinger1} W. Kohn and J.M Luttinger derived an
expression for how electrons and holes would move in the presence of a localized
perturbation to the periodic field, in particular the period fields of the Si
and Ge semiconductors. These semiconductors do not have a simple conduction band
with a single energy minima -- instead they have 6(Si) or 8(Ge) equivalent
minima lying along different directions in $\bm{k}$-space. In a later paper that
year\cite{KohnLuttinger2}, Kohn and Luttinger (hereinafter referred in this
section as KL) presented an application of the effective mass theory to donor
states in silicon. An excess electron, bound by an impurity in silicon with an
excess positive charge, becomes trapped in the crystal. Such a system can be
referred to as an electron donor, conventionally just called a donor. In
silicon, the focus is on the Group V substitutional impurities, such as
phosphorous, which feature an outer electron shell that matches the outer shell
of the silicon atom. This allows the phosphorous to participate in the crystal
structure as though it were silicon. The net effect to the system through the
introduction of this impurity is some minor deformation of the lattice at or
near the donor site, and most importantly that such an atom participating in
silicon bonds will have an excess positive charge that isn't satisfied by the
electrons from its crystalline neighbors. This creates the situation above,
where an excess electron is localized in the region of the donor. While deep
donors exist, we limit our focus here to "shallow" donors. We will save further
discussion of "shallowness" and its impact on the definition of the effective
mass approximation in Section \ref{Shallow}.

KL examined a single-valley model of the donor electron wavefunction that went
as so
\begin{equation}
\Psi_{\bm{r}}
    =
        F_{\bm{r}}\e^{\im \bm{k}_{q}\cdot r}u_{0\bm{k}_{q}\bm{r}}
\end{equation}
Above $\bm{k}_{q}$ is the point in $k$-space where the energy minima of the
lowest conduction band are located. $F$ represents an envelope function that
introduces spatial localization of the wavefunction. We can see that the
wavefunction takes the form of a conduction band wavefunction attenuated and
made a bound state by a leading envelope function factor.

KL did not extend their model to consider intervalley coupling, and so this
model's ground state manifold is 6-fold degenerate. In actuality, intervalley
couplings lift this 6-fold degeneracy to create a 1-fold $A_{1}$ state, and
2-fold $E$ state, and a 3-fold $T_{2}$ state, with $A_{1}$, $E$ and $T_{2}$
referring to the particular symmetries of the overall state. For now, we will
table the discussion of multi-valley states. We will return to this topic in the
next section. For now, we will discuss their specification of $F$. In the
"shallow" limit, when the effective mass approximation can be applied, $F$ will
follow
\begin{equation}
    \left[
            -
        \frac{\hbar^{2}}{2m_{\parallel}}\frac{\partial^{2}}{\partial x^{2}}
            -
        \frac{\hbar^{2}}{2m_{\parallel}}\frac{\partial^{2}}{\partial y^{2}}
            -
        \frac{\hbar^{2}}{2m_{\perp}}\frac{\partial^{2}}{\partial z^{2}}
            -
        \frac{e^2}{4\pi\epsilon\sqrt{x^2+y^2+z^2}}
    \right]
    F_{\bm{r}}
    =
    E F_{\bm{r}}
\label{defintion:SV-envelope}
\end{equation}
This is a nonseparable partial differential equation, and no closed form exact
solution is known. However, we can make an educated guess as to the form of the
envelope, and KL predicted that the envelope could be approximated as
\begin{equation}
F_{\bm{r}}
    \approx
F_{\bm{r}}^{\prime}
    \equiv
        \frac{1}{\sqrt{\pi a^{2}b}}
        \e^{-\sqrt{\frac{x^2+y^2}{a^2}+\frac{z^{2}}{b^2}}}.
\end{equation}
I note that the above function is normalized in the sense that
\begin{equation}
\int\diff\bm{r}\,F_{\bm{r}}^{*}F_{\bm{r}} = 1.
\end{equation}
As such, one can calculate the expectation energy as 
\begin{align}
E_{ab} 
    & = 
        \frac{\hbar^{2}}{2m_{\parallel}}
        \int\diff\bm{r}\,
        F_{\bm{r}}^{*}
        \left[
                -
            \frac{\partial^{2}}{\partial x^{2}}
                -
            \frac{\partial^{2}}{\partial y^{2}}
                -
            \gamma\frac{\partial^{2}}{\partial z^{2}}
                -
            \frac{e^2}{4\pi\epsilon r}
        \right]
        F_{\bm{r}}
,\\
    & =
        \frac{\mathfrak{E}\mathfrak{r}^{2}}{3a^{2}}
        \left[
            2+\gamma\left(\frac{a}{b}\right)^2
                -
            6
            \left(\frac{a}{b}\right)
            \left(\frac{a}{\mathfrak{r}}\right)
            \frac{
                \text{ArcTan}\left(
                    \sqrt{\left(\frac{a}{b}\right)^{2}-1}
                \right)
            }
            { \sqrt{\left(\frac{a}{b}\right)^{2}-1} }
        \right]
\label{equation:KL-energy}
.
\end{align}
The quantities $\mathfrak{E}$ and $\mathfrak{r}$ are system units. Their
definition may be found in Appendix \ref{appendix:units}. $\gamma =
m_{\parallel}/m_{\perp}$ is the effective mass anisotropy.  We may implement $a$
and $b$ as unitful variational parameters, however if a dimensionless
representation is desired, we may take $a,b \rightarrow \mathfrak{r}a,
\mathfrak{r}b$. If that is done so, the $\mathfrak{r}$ drops out of the equation
above, and the minimization problem becomes purely numerical.

It can be shown by applying the calculus of variations to quantum mechanics that
an energy eigenstate is an energetic stationary point in state space, and in
particular is a minimum of energy. If selecting from a parameterized subspace,
the problem goes from the calculus of variations to normal real number calculus.
In the \emph{variational method}, we can extend this principle and suggest that
the best approximation to the energy eigenstate consists of selecting a trial
wavefunction from a parameterized set by optimizing the parameters to minimize
the energy. In their paper, KL suggest a few more trial wavefunctions of the
various excited states of the silicon donor as well as the ground state. KL
found reasonable, but not exact, agreement with the excited states.

Using a value of $\gamma = 0.207901$ in our , we can minimize
\eqref{equation:KL-energy} by varying $a$ and $b$. If we do so, we find
\begin{align}
    E_{\text{min}} &= -1.5653\mathfrak{E},\\
    a_{\text{min}} &= 0.74832\mathfrak{r},\\
    b_{\text{min}} &= 0.43001\mathfrak{r},\\
\end{align}
This is a ground state energy of $E_{\text{min}} = \SI{-31.1}{\meV}$.

In Chapter \ref{chapter:results} we will explore in greater detail how our
software package can directly replicate and extend these results. In particular,
we wish to examine the implications of introducing multi-valley interactions.

While we can continue examining the other variational wavefunctions employed by
KL in their exploration of the excited donor states of silicon, we find it
convenient at this juncture to move forward. In the next section, we will
examine a significant improvement upon the bespoke single-state variational
method and discuss how R.A. Faulker used the Ritz method.